#pragma once
#include "Parameter.h"
#include "CMesh.h"

class ParameterMgr
{
	typedef enum { GLAREA, DATA, DRAWER, NOR_SMOOTH, POISSON }ParaType;

public:
	ParameterMgr(void);
	~ParameterMgr(void);

	void setGlobalParameter(QString paraName, Value& val);


private:

	
public:
	RichParameterSet glarea;
	
private:
};

extern ParameterMgr global_paraMgr;