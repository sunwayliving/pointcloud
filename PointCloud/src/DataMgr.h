#ifndef _DATAMGR_H_
#define _DATAMGR_H_
#include <QString>
#include <wrap/io_trimesh/import.h>
#include <wrap/io_trimesh/export.h>
#include <vcg/complex/trimesh/update/normal.h>

#include "CMesh.h"
#include "GlobalFun.h"

class DataMgr{
private:
	DataMgr() {};

public:
	static DataMgr* getInstance(){
		static DataMgr *instance = new DataMgr();
		return instance;
	}

	~DataMgr() {};

public:
	//utility function
	void loadPlyToOriginal(QString fileName);

public:
	CMesh* getOriginal() { return &original; }
	CMesh* getSample()   { return &sample; }

private:
	CMesh original;
	CMesh sample;
};

#endif