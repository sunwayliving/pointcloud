#include "GLArea.h"

GLArea::GLArea(QWidget *parent)
{
	setMouseTracking(true);

	CVertex v;
	cout << "Memory Size of each CVertex: " << sizeof(v) << endl;
}

GLArea::~GLArea()
{
	
}

//Initial 
void GLArea::initializeGL()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_COLOR_MATERIAL);

	glShadeModel(GL_SMOOTH);
	glShadeModel(GL_FLAT);

	//glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST ); 
	GLfloat mat_ambient[4] = { 0.1745, 0.01175, 0.01175, 1.0 };
	GLfloat mat_diffuse[4] = { 0.61424, 0.04136, 0.04136, 1.0 };
	GLfloat mat_specular[] = { 0.727811, 0.626959, 0.626959, 1.0 };
	GLfloat shininess = 0.6 * 128;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, shininess);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT);
	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	glDisable(GL_BLEND);
	glEnable(GL_NORMALIZE);

	glDisable(GL_CULL_FACE);
	glColor4f(1, 1, 1, 1);

	glEnable(GL_LIGHTING);

	initLight();


	trackball.center = Point3f(0, 0, 0);
	trackball.radius = 1;

	// force to open anti
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glBlendFunc(GL_ONE, GL_ZERO);  
	glEnable(GL_BLEND);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

	glDisable(GL_LIGHTING);

	glLoadIdentity();
}

void GLArea::initLight()
{
	GLColor color_ambient(55./255.f, 55./255, 55./255);// (para->getColor("Light Ambient Color"));
	float ambient[4] = { color_ambient.r, color_ambient.g, color_ambient.b, 1.0 };
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);

	GLColor color_diffuse(160 / 255., 160 / 255., 164 / 255.);// (para->getColor("Light Diffuse Color"));
	float diffuse[4] = { color_diffuse.r, color_diffuse.g, color_diffuse.b, 1.0 };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);

	GLColor color_specular(1.0f, 1.0f, 1.0f);// (para->getColor("Light Specular Color"));
	float specular[4] = { color_specular.r, color_specular.g, color_specular.b, 1.0 };
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specular);

}

void GLArea::resizeGL(int w, int h)
{
	glViewport(0, 0, (GLint)w, (GLint)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float r = w / (float)h;
	gluPerspective(60, r, 0.1, 10);
	glMatrixMode(GL_MODELVIEW);
}

void GLArea::paintGL()
{
	GLColor color(1, 1, 1); // global_paraMgr.drawer.getColor("Background Color"));
	glClearColor(color.r, color.g, color.b, 1);

	Point3f lightPos = vcg::Point3f(-4.0, -4.0, -4.0f);//para->getPoint3f("Light Position");
	float lpos[4];
	lpos[0] = lightPos[0];
	lpos[1] = lightPos[1];
	lpos[2] = lightPos[2];
	lpos[3] = 0;
	glLightfv(GL_LIGHT0, GL_POSITION, lpos);
	lpos[0] = -lightPos[0];
	lpos[1] = -lightPos[1];
	lpos[2] = -lightPos[2];
	lpos[3] = 0;
	glLightfv(GL_LIGHT1, GL_POSITION, lpos);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glLoadIdentity();
	gluLookAt(0, 0, -3, 0, 0, 0, 0, 1, 0);
}


void GLArea::drawLightBall()
{
  // ============== LIGHT TRACKBALL ==============
  // Apply the trackball for the light direction

  glPushMatrix();
  trackball_light.GetView();
  trackball_light.Apply();

  static float lightPosF[]={0.0,0.0,1.0,0.0};
  glLightfv(GL_LIGHT0,GL_POSITION,lightPosF);
  static float lightPosB[]={0.0,0.0,-1.0,0.0};
  glLightfv(GL_LIGHT1,GL_POSITION,lightPosB);

  if (!(isDefaultTrackBall()))
  {
    glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
    glColor3f(1,1,0);
    glDisable(GL_LIGHTING);
    const unsigned int lineNum=3;
    glBegin(GL_LINES);
    for(unsigned int i=0;i<=lineNum;++i)
      for(unsigned int j=0;j<=lineNum;++j) {
        glVertex3f(-1.0f+i*2.0/lineNum,-1.0f+j*2.0/lineNum,-2);
        glVertex3f(-1.0f+i*2.0/lineNum,-1.0f+j*2.0/lineNum, 2);
      }
      glEnd();
      glPopAttrib();
  }
  glPopMatrix();
}


void GLArea::wheelEvent(QWheelEvent *e)
{

}

void GLArea::mouseMoveEvent(QMouseEvent *e)
{

}

void GLArea::mousePressEvent(QMouseEvent *e)
{

}

void GLArea::mouseReleaseEvent(QMouseEvent *e)
{

}

void GLArea::keyReleaseEvent(QKeyEvent *e)
{

}

void GLArea::keyPressEvent(QKeyEvent *e)
{

}