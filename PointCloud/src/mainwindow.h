#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

#include <Qtwidgets/QApplication>
#include <Qtwidgets/QFileDialog>

#include "ui_mainwindow.h"
#include "GLArea.h"
#include "DataMgr.h"

//MainWindow类主要用来消息响应
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~MainWindow();

private:
	void init();
	void initWidgets();
	void initConnect();
	void iniStatusBar();
	void createActionGroups();
	void keyPressEvent(QKeyEvent *e);
	void keyReleaseEvent(QKeyEvent *e);

private slots:
	void updateStatusBar();
	/*void dropEvent(QDropEvent * event);
	void dragEnterEvent(QDragEnterEvent *);*/

	void openFile();
	void saveFile();
	
private:
	GLArea* area;
	QString strTitle;

private:
	Ui::mainwindowClass ui;
};

#endif // MAINWINDOW_H