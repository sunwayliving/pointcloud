#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	cout << "MainWindow constructed" << endl;
	ui.setupUi(this);
	area = new GLArea(this);
	setCentralWidget(area);

	init();
	initWidgets();
	createActionGroups();
	iniStatusBar();
	initConnect();

	//area->loadDefaultModel();
	//to avoid unclear UI at start
}

MainWindow::~MainWindow()
{
	if (area) delete area;
	area = NULL;
}

void MainWindow::initWidgets()
{

}

void MainWindow::initConnect()
{
	if (!connect(area, SIGNAL(needUpdateStatus()), this, SLOT(updateStatusBar()))){
		cout << "can not connect signal" << endl;
	}

	connect(ui.actionImport_Ply, SIGNAL(triggered()), this, SLOT(openFile()));
}

void MainWindow::iniStatusBar()
{

}

void MainWindow::updateStatusBar()
{
	update();
	repaint();
}


void MainWindow::createActionGroups()
{

}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
	area->keyPressEvent(e);
}

void MainWindow::keyReleaseEvent(QKeyEvent *e)
{
	area->keyReleaseEvent(e);
}

void MainWindow::init()
{
	strTitle = "Area Consistent Deformation";

	//paras = &global_paraMgr;
}

void MainWindow::openFile()
{
	QString file = QFileDialog::getOpenFileName(this, "select a ply file", "", "*.ply");
	if (!file.size()) return;

	DataMgr::getInstance()->loadPlyToOriginal(file);
}

void MainWindow::saveFile()
{

}
