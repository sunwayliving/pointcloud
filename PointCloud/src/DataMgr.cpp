#include "DataMgr.h"

//ctor and dtor are defined in *.h file

void DataMgr::loadPlyToOriginal(QString fileName){
	GlobalFun::clearMesh(original);

	cout << fileName.toStdString() << endl;

	int mask = tri::io::Mask::IOM_ALL;
	int err = tri::io::Importer<CMesh>::Open(original, fileName.toStdString().data(), mask);
	if (err)
	{
		cout << "Failed reading mesh: " << err << "\n";
		return;
	}
	cout << "points loaded\n";

	//vcg::tri::UpdateNormals<CMesh>::PerVertex(original);
	vcg::tri::UpdateNormal<CMesh>::PerVertex(original);

	CMesh::VertexIterator vi;
	int idx = 0;
	for (vi = original.vert.begin(); vi != original.vert.end(); ++vi)
	{
		vi->is_original = true;
		vi->m_index = idx++;
		vi->N().Normalize();
		original.bbox.Add(vi->P());
	}
	original.vn = original.vert.size();
}