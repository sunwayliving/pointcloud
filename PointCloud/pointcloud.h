#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include <QtWidgets/QMainWindow>
#include "GeneratedFiles\ui_pointcloud.h"

class PointCloud : public QMainWindow
{
	Q_OBJECT

public:
	PointCloud(QWidget *parent = 0);
	~PointCloud();

private:
	Ui::PointCloudClass ui;
};

#endif // POINTCLOUD_H
